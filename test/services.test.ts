'use strict';

import 'mocha';
import { assert } from 'chai';
import { getCatImg } from '../src/commons/services';

describe('Service tests', () => {
    it('getFactText', async () => {
        const fact = await getCatImg();
        assert.isNotNull(fact);
        assert.isNotNull(fact.url);
    });
});