import { Directive } from "ask-sdk-model";
import { Cat } from "../commons/services";
import  aplDoc from "./apls/apl";

const getAplParam = (imgUrl) => {
    return {
        "bodyTemplate7Data": {
            "type": "object",
            "objectId": "bt7Sample",
            "title": "Chats mignons",
            "image": {
                "contentDescription": null,
                "smallSourceUrl": null,
                "largeSourceUrl": null,
                "sources": [
                    {
                        "url": imgUrl,
                        "size": "small",
                        "widthPixels": 0,
                        "heightPixels": 0
                    },
                    {
                        "url": imgUrl,
                        "size": "large",
                        "widthPixels": 0,
                        "heightPixels": 0
                    }
                ]
            },
            "logoUrl": "https://chatsmignons.s3-eu-west-1.amazonaws.com/cat_small.png",
        }
    }
}

export const apl = (fact: Cat): Directive => {
    return {
        type: 'Alexa.Presentation.APL.RenderDocument',
        document: aplDoc,
        datasources: getAplParam(fact.url)
    };
}