import { ui } from 'ask-sdk-model';
import { getOne } from './utils/utils';


export const Constants = {
    messages: {
        welcome: () => {
            return 'bienvenue sur chat mignon.'; 
        },
        next: () => {
            return 'Et voilà une autre image.';
        },
        error: () => {
            return 'Il semblerait que chat mignon a un problème. Ré-essais dans un instant.';
        },
        help: (): string => {
            return "Avec chats mignons, fondez devant des photos de chats. Dites simplements, `Alexa, démarre chats mignons` et accédez à des images de chats très mignons... Souhaitez vous une nouvelle image?";
        },
        not_supported: () => {
            return "Dommage. Pour profiter de nos chats mignons, il faut disposer d'un écran. A bientôt sur un autre appareil Alexa.";
        },
        reprompt: () => {
            return "Souhaitez vous une autre image de chat mignon?"
        },
        bye: () => {
            return "Miaou vous dit à bientôt";
        }
    }
}