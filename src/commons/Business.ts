import { Constants } from './Constants';
import { getCatImg } from './services.js';
import { HandlerInput } from 'ask-sdk';
import { apl } from '../skill/template';
import { SkillInputUtils } from '../skill/SkillInputUtils';


export const playFact = async (input: HandlerInput, isWelcome: boolean) => {
    let msg;
    if (isWelcome) {
        msg = Constants.messages.welcome();
    } else {
        msg = Constants.messages.next();
    }
    if (new SkillInputUtils(input).hasApl()) {
        const fact = await getCatImg();
        console.log("cat img", fact);
        input.responseBuilder.speak(msg + ' '+ Constants.messages.reprompt())
        input.responseBuilder.addDirective(apl(fact));
        input.responseBuilder.reprompt(Constants.messages.reprompt())
    } else {
        input.responseBuilder.speak(Constants.messages.not_supported())
            .withShouldEndSession(true);
    }
}   
