import * as request from 'request-promise-native';

const urlImg = 'https://api.thecatapi.com/v1/images/search';

/*
[{"id":"271","fact":"http:\/\/www.chucknorrisfacts.fr\/img\/upload\/q48da669cd83d2.jpg","date":"1222288034","vote":"5494","points":"21503"}]
*/
export interface Cat {
    id: string;
    url: string;
}

const getCat = async (url: string): Promise<Cat> => {
    var options = {
        uri:url,
        json: true
    };

    const cats = await request.get(options);
    return cats[0];
}

export const getCatImg = async (): Promise<Cat> => {
    return getCat(urlImg);
}
